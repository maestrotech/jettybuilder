/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder.factory

import com.maestro.groovy.builders.jettyBuilder.BNode
import com.maestro.groovy.builders.jettyBuilder.node.ServletContextHandlerNode
import groovy.util.logging.Log4j
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder

import javax.servlet.Servlet

@SuppressWarnings("GroovyUnusedDeclaration")
@Log4j
class ServletContextHandlerFactory extends BaseFactory<ServletContextHandler> {

  ServletContextHandlerFactory( ) {
    super( ServletContextHandler )
    childTypes = [ ( ServletHolder ): null, ( Servlet ): null ]
  }

  protected BNode<ServletContextHandler> onNewInstance( String name, Object value, Map map, FactoryBuilderSupport fbs ) {
    def node = new ServletContextHandlerNode( name, value, map, clazz )
    node.ignoreAttributes = [ "servlets", "useSessions", "useSecurity" ]
    node
  }

  /**
   * Add children - override for special handling
   * @param node
   * @return
   */
  def addChildren( BNode<ServletContextHandler> node ) {
    def servlets = node.attributes.remove( "servlets" ) as Map<String, Object>
    log.info servlets
    if ( servlets ) {
      servlets.each { path, _item ->
        log.info "$node: adding servlet: $_item -> $path"
        def item = _item instanceof BNode ? _item.value : _item
        if ( item instanceof String ) {
          node.value.addServlet item, path
        } else if ( item instanceof ServletHolder ) {
          node.value.addServlet item, path
        } else if ( Servlet.isAssignableFrom( item.class ) ) {
          node.value.addServlet item, path
        }
      }
    }
    super.addChildren node
  }
}

