/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder

import org.apache.log4j.Logger

@SuppressWarnings("GroovyUnusedDeclaration")
abstract class ScriptBaseClass extends Script {
  static def LOG = Logger.getLogger( ScriptBaseClass )
  boolean bindingDone = false

  static getNestedProperty( Object object, String property ) {
    property.tokenize( '.' ).inject( object, { obj, prop -> obj[ prop ] } )
  }

  static void setNestedProperty( Object object, String property, Object val ) {
    def parts = property.tokenize( '.' )
    def last = parts[ -1 ]
    def x = parts[ 0..-2 ].inject( object, { obj, prop -> obj[ prop ] } )
    x."$last" = val
  }

  public Object invokeMethod( String name, Object args ) {
    LOG.info "invokeMethod $name"
    super.invokeMethod( name, args )
  }

  public Object getProperty( String property ) {
    if ( !( "binding".equals( property ) || bindingDone ) ) {
      bindProperties()
    }

    LOG.info "get $property"
    if ( "ext".equals( property ) ) return this

    try {
      property.contains( "." ) ? getNestedProperty( super, property ) : super.getProperty( property )
    }
    catch ( MissingPropertyException ignored ) {
      LOG.info "missing property: $property"
      null
    }
  }

  void setProperty( String prop, Object val ) {
    LOG.info "set $prop: $val"
    if ( prop.contains( "." ) ) {
      setNestedProperty this, prop, val
    } else {
      super.setProperty prop, val
    }
  }

  void ext( Closure c ) {
    c.delegate = this
    c.call()  // initialize script variables first
    bindProperties()
  }

  void bindProperties( ) {
    //avoid recursive calls
    bindingDone = true
    //see if caller provided properties (can overwrite script props)
    def b = binding
    if ( b.hasVariable( "__props" ) ) {
      def props = b.getVariable( "__props" ) as Map<String, Object>
      props?.each { this.setProperty it.key, it.value }
    }
  }
}
