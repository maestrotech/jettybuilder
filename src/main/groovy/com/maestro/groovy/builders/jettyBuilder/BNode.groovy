/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder

import com.vperi.groovy.lang._
import org.apache.log4j.Logger
import org.codehaus.groovy.runtime.InvokerHelper

/**
 *  The type Graph node.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
class BNode<T> {
  static def LOG = Logger.getLogger( BNode )
  String name
  Class<T> nodeClass
  Map attributes = [ : ]
  Object literal
  BNode parent
  List<BNode> children = [ ]
  T value
  boolean initialized = false
  boolean isBean = false
  def ignoreAttributes = [ ]

  /**
   *  Instantiates a new Graph node.
   *
   * @param name the name
   * @param literal the literal
   * @param map the map
   */
  BNode( Object name, Object literal, Map map, Class<T> c ) {
    this.name = name
    this.literal = literal
    nodeClass = c
    this.attributes.putAll map
  }

  String toString( ) {
    "${this.name}" + ( this.attributes.size() ? " ${this.attributes}" : "" ) + " " + this.value?.class?.simpleName
  }

  /**
   *  Create object that's being proxied by this node
   *
   * @param args the args
   */
  T completeInit( Object... args ) {
    if ( !initialized ) {
      LOG.info "$name: completeInit: $args / $literal"
      if ( !value ) {
        value = [ args.length > 0, literal != null ].match {
          when( [ true, _ ] ) then { nodeClass.newInstance args }
          when( [ false, true ] ) then { nodeClass.newInstance( literal instanceof BNode ? literal.value : literal ) }
          otherwise { nodeClass.newInstance() }
        } as T
      }
      applyAttributes()
      applyBeans()
      initialized = true
    }
    value
  }

  /**
   *  Apply attributes.
   */
  def applyAttributes( ) {
    if ( !value ) return

    attributes.findAll { !( it.key in ignoreAttributes ) }.each {
      def val = it.value instanceof BNode ? it.value.value : it.value
      LOG.info "$name: setting $it.key:$val"
      value."$it.key" = it.value
    }
  }

  /**
   *  Find child of type.
   *
   * @param childClass the child class
   * @return the graph node
   */
  public <C> BNode<C> findChildOfType( Class<C> childClass ) {
    children.find { childClass.isAssignableFrom it.nodeClass }
  }

  /**
   *  Find children of type.
   *
   * @param childClass the child class
   * @return the list
   */
  public <C> List<BNode<C>> findChildrenOfType( Class<C> childClass ) {
    children.findAll { childClass.isAssignableFrom it.nodeClass }
  }

  /**
   *  On set child.
   *
   * @param bNode the b node
   * @param factoryBuilderSupport the factory builder support
   */
  void onSetChild( final BNode bNode, final FactoryBuilderSupport factoryBuilderSupport ) {
    children.add bNode
  }

  /**
   *  Apply beans.
   */
  void applyBeans( ) {
    children.findAll { it.isBean }.each {
      LOG.info "addBean: ${it.value.class.name} on $this"
      InvokerHelper.invokeMethod value, "addBean", it.value
    }
  }
}
