/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder.factory

import org.eclipse.jetty.server.HttpConfiguration

@SuppressWarnings("GroovyUnusedDeclaration")
class HttpConfigurationFactory extends BaseFactory<HttpConfiguration> {

  /**
   *  Instantiates a new Http connection factory factory.
   */
  HttpConfigurationFactory( ) {
    super( HttpConfiguration )
    childTypes = [
        ( HttpConfiguration ): null,
        ( HttpConfiguration.Customizer ): [ addChld: "addCustomizer" ]
    ]
  }
}
