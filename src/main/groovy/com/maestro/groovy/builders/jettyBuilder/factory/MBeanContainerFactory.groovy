/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder.factory

import com.maestro.groovy.builders.jettyBuilder.BNode

@SuppressWarnings("GroovyUnusedDeclaration")
class MBeanContainerFactory extends BeanFactory2 {

  protected BNode<Object> onNewInstance( String name, Object literal, Map map, FactoryBuilderSupport fbs ) {
    assert literal != null, "MBeanServer instance must be provided as literal argument"
    this.clazz = literal.class
    def node = new BNode( name, null, map, clazz )
    node.isBean = true
    node.value = literal
    node
  }
}