/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder.factory

import com.maestro.groovy.builders.jettyBuilder.BNode
import com.vperi.groovy.lang._
import org.eclipse.jetty.server.Handler
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.server.handler.HandlerList
import org.eclipse.jetty.util.thread.ThreadPool

@SuppressWarnings("GroovyUnusedDeclaration")
class ServerFactory extends BaseFactory<Server> {

  /**
   *  Instantiates a new Server factory.
   */
  ServerFactory( ) {
    super( Server )
    childTypes = [ ( Object ): null ]
  }

  protected void completeInit( BNode parent, BNode<Server> node, FactoryBuilderSupport builder ) {
    Server server = null

    node.with {
      def threadPool = findChildOfType( ThreadPool )
      def port = attributes.remove( "port" ) as Integer
      String host = attributes.remove( "host" )

      server = [ threadPool != null, host != null, port != null ].match {
        when( [ false, false, false ] ) then { completeInit() }
        when( [ true, _, _ ] ) then { completeInit threadPool.value }
        when( [ _, false, true ] ) then { completeInit port }
        when( [ _, true, false ] ) then { throw new RuntimeException( "port must be defined if host is defined" ) }
        otherwise { completeInit new InetSocketAddress( host, port ) }
      } as Server

      findChildrenOfType( ServerConnector ).each {
        def connector = it.completeInit( server )
        server.addConnector connector
      }

      def handlers = findChildrenOfType( Handler ).collect { it.value }
      assert handlers.size(), "No handlers!"
      if ( handlers.size() > 1 ) {
        def h = handlers
        handlers = new HandlerList()
        handlers.setHandlers h as Handler[]
      } else {
        handlers = handlers[ 0 ]
      }

      server.setHandler handlers

      builder.addDisposalClosure {
        log.infonfo "disposing server"
        server.finalize()
      }
    }
    server
  }
}