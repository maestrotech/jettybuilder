/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder

import com.maestro.groovy.builders.jettyBuilder.factory.*
import org.apache.log4j.Logger
import org.codehaus.groovy.control.CompilerConfiguration
import org.eclipse.jetty.rewrite.handler.RewriteHandler
import org.eclipse.jetty.security.HashLoginService
import org.eclipse.jetty.server.ForwardedRequestCustomizer
import org.eclipse.jetty.server.HttpConfiguration
import org.eclipse.jetty.server.HttpConnectionFactory
import org.eclipse.jetty.server.SecureRequestCustomizer
import org.eclipse.jetty.server.handler.*
import org.eclipse.jetty.server.session.SessionHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.eclipse.jetty.util.ssl.SslContextFactory
import org.eclipse.jetty.util.thread.ExecutorThreadPool
import org.eclipse.jetty.util.thread.QueuedThreadPool
import org.eclipse.jetty.webapp.WebAppContext

import java.lang.management.ManagementFactory

import static org.codehaus.groovy.control.customizers.builder.CompilerCustomizationBuilder.withConfig

@SuppressWarnings("GroovyUnusedDeclaration")
class JettyBuilder extends FactoryBuilderSupport {
  private static LOG = Logger.getLogger( JettyBuilder )
  public static final String DELEGATE_PROPERTY_OBJECT_ID = "_delegateProperty:id";
  public static final String DEFAULT_DELEGATE_PROPERTY_OBJECT_ID = "id";

  JettyBuilder( boolean init = true ) {
    super( init )
    this[ DELEGATE_PROPERTY_OBJECT_ID ] = DEFAULT_DELEGATE_PROPERTY_OBJECT_ID
  }

  void registerServer( ) {
    registerFactory "server", new ServerFactory()
  }

  void registerThreadPools( ) {
    registerFactory "queuedThreadPool", new BaseFactory( QueuedThreadPool )
    registerFactory "executorThreadPool", new BaseFactory( ExecutorThreadPool )
  }

  void registerNodes( ) {
    registerFactory "connector", new ServerConnectorFactory()
    registerFactory "sslContext", new BaseFactory( SslContextFactory )
    registerFactory "httpConnection", new BaseFactory( HttpConnectionFactory,
        [ childTypes: [ ( HttpConfiguration ): null ] ] )
    registerFactory "sslConnection", new SslConnectionFactoryFactory()
    registerFactory "httpConfig", new HttpConfigurationFactory()
    registerFactory "secureRequestCustomizer", new BaseFactory( SecureRequestCustomizer )
    registerFactory "forwardRequestCustomizer", new BaseFactory( ForwardedRequestCustomizer )
    registerFactory "bean", new BeanFactory2()
    registerFactory "mBeanContainer", new MBeanContainerFactory()

    //object id delegate, for propertyNotFound
    addAttributeDelegate JettyBuilder.&objectIDAttributeDelegate
    addAttributeDelegate JettyBuilder.&clientPropertyAttributeDelegate

  }

  void registerHandlerContainers( ) {
    registerFactory "handlerWrapper", new HandlerWrapperFactory()
    registerFactory "handlersCollection", new HandlerCollectionFactory( HandlerCollection )
    registerFactory "handlerList", new HandlerCollectionFactory( HandlerList )
    registerFactory "contextHandlerCollection", new HandlerCollectionFactory( ContextHandlerCollection )
  }

  void registerHandlers( ) {
    registerFactory 'contextHandler', new BaseFactory( ContextHandler )
    registerFactory 'defaultHandler', new BaseFactory( DefaultHandler )
    registerFactory 'errorHandler', new BaseFactory( ErrorHandler )
    registerFactory 'movedContextHandler', new BaseFactory( MovedContextHandler )
    registerFactory 'resourceHandler', new BaseFactory( ResourceHandler )
    registerFactory 'rewriteHandler', new BaseFactory( RewriteHandler )
    registerFactory 'servletContextHandler', new ServletContextHandlerFactory()
    registerFactory 'servletHandler', new ServletHandlerFactory()
    registerFactory 'sessionHandler', new BaseFactory( SessionHandler )
    registerFactory 'shutdownHandler', new BaseFactory( ShutdownHandler )
    registerFactory 'statisticsHandler', new BaseFactory( StatisticsHandler )
    registerFactory 'webAppContext', new BaseFactory( WebAppContext )
    registerFactory 'servletHolder', new BaseFactory( ServletHolder )
  }

  void registerSecurity( ) {
    registerFactory 'hashLoginService', new BeanFactory2( HashLoginService )

  }

  /**
   *  Build object.
   *
   * @param c the c
   * @return the object
   */
  public Object build( Closure c ) {
    c.resolveStrategy = Closure.DELEGATE_ONLY
    c.delegate = this
    c().value
  }

  /**
   *  From script.
   *
   * @param fileName the file name
   * @param args the args
   */
  static def fromScript( String fileName, Map args = [ : ] ) {
    def builder = new JettyBuilder()
    def conf = new CompilerConfiguration()
    withConfig( conf ) {
      imports {
        normal "java.lang.management.ManagementFactory"
      }
    }
    conf.scriptBaseClass = 'ScriptBaseClass'

    def name = new File( fileName ).name.split( "\\." )[ 0 ]
    def props = [
        jetty: builder,
        ManagementFactory: ManagementFactory,
        LOG: Logger.getLogger( name ) ] + args
    builder.setVariable "__props", props

    def shell = new GroovyShell( this.class.classLoader, builder, conf )
    shell.evaluate( new File( fileName ) )?.value
  }

  public static objectIDAttributeDelegate( def builder, def node, def attributes ) {
    def idAttr = builder.getAt( DELEGATE_PROPERTY_OBJECT_ID ) ?: DEFAULT_DELEGATE_PROPERTY_OBJECT_ID
    def theID = attributes.remove( idAttr )
    if ( theID ) {
      builder.setVariable( theID, node )
      if ( node ) {
        try {
          if ( !node.name ) node.name = theID
        }
        catch ( MissingPropertyException mpe ) {
          LOG.warn mpe
        }
      }
    }
  }

  /**
   *  Client property attribute delegate.
   *
   * @param builder the builder
   * @param _node the _ node
   * @param attributes the attributes
   */
  public static clientPropertyAttributeDelegate( def builder, def _node, def attributes ) {
    if ( !_node instanceof BNode ) return

    def node = _node as BNode
    def clientPropertyMap = attributes.remove( "clientProperties" )
    clientPropertyMap.each { key, value ->
      node.attributes.put key, value
    }

    attributes.findAll { it.key =~ /clientProperty(\w)/ }.each { key, value ->
      attributes.remove( key )
      node.attributes.put key - "clientProperty", value
    }
  }

  Object getProperty( String name ) {
    try {
      return super.getProperty( name )
    }
    catch ( MissingPropertyException ignored ) {
      LOG.info "missing property $name"
      null
    }
  }
}
