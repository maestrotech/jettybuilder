/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder.factory

import com.maestro.groovy.builders.jettyBuilder.BNode
import com.maestro.groovy.builders.jettyBuilder.node.ServerConnectorNode
import org.eclipse.jetty.server.HttpConnectionFactory
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.server.SslConnectionFactory
import org.eclipse.jetty.util.ssl.SslContextFactory

@SuppressWarnings("GroovyUnusedDeclaration")
class ServerConnectorFactory extends BaseFactory<ServerConnector> {

  /**
   *  Instantiates a new Server connector factory.
   */
  ServerConnectorFactory( ) {
    super( ServerConnector )
    childTypes = [ ( SslContextFactory ): null, ( HttpConnectionFactory ): null, ( SslConnectionFactory ): null ]
    initAfterParent = true
  }

  /**
   * onNewInstance
   *
   * Override to supply specialized node
   *
   * @param name
   * @param value
   * @param map
   * @param fbs
   * @return
   */
  protected BNode onNewInstance( String name, Object value, Map map, FactoryBuilderSupport fbs ) {
    new ServerConnectorNode( name, value, map, clazz )
  }
}
