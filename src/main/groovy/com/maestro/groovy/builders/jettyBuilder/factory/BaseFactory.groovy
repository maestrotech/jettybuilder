/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.maestro.groovy.builders.jettyBuilder.factory
import com.maestro.groovy.builders.jettyBuilder.BNode
import groovy.util.FactoryBuilderSupport as FBS
import org.codehaus.groovy.runtime.InvokerHelper
/**
 *  The type Base factory.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
class BaseFactory<T> extends AbstractFactory {
//  protected LOGGER LOG

  /**
   * Value type
   */
  Class<T> clazz

  /**
   * List of valid value types for child nodes
   */
  Map<Class, Map> childTypes = [ : ]

  /**
   * Child method for adding children
   */
  Map<Class, Map<String, String>> childMethods = [ : ]

  /**
   * Initialize current node after parent is initialized.
   * Default behavior is to initialize child nodes before parent
   */
  boolean initAfterParent = false

  /**
   *  Instantiates a new Base factory.
   */
  BaseFactory( Class<T> clazz, Map map = null ) {
    this.clazz = clazz
    map?.each {
      this."${it.key}" = it.value
    }
   // LOG = new LOGGER( this.class )
  }

  /**
   * Called by FBS to create a new node instance
   *
   * @param fbs
   * @param _name
   * @param _value
   * @param map
   * @return
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  @Override
  Object newInstance( FBS fbs, Object _name, Object _value, Map map ) throws InstantiationException,
      IllegalAccessException {
    def ignore = [ "id" ]
    def m = [ : ]

//    log.info "newInstance: $_name[$_value, $map]"

    def iter = map.entrySet().iterator()

    while ( iter.hasNext() ) {
      def item = iter.next()
      if ( !( item.key in ignore ) ) {
        m[ item.key ] = item.value
        iter.remove()
      }
    }
    onNewInstance _name as String, _value, m, fbs
  }

  /**
   *  Handle new instance.
   *
   * @param name the name
   * @param value the literal
   * @param map the map
   * @param fbs the fbs
   * @return the object
   */
  protected BNode<T> onNewInstance( String name, Object value, Map map, FBS fbs ) {
    new BNode( name, value, map, clazz )
  }

  /**
   * Called by FBS to notify child of parent
   * @param builder
   * @param _parent
   * @param _child
   */
  @Override
  public void setParent( FBS builder, Object _parent, Object _child ) {
    def (BNode<T> child, BNode<T> parent) = sanityCheck( _child, _parent )
    assert child.isBean || child.nodeClass == clazz, "Bad parent or child found"
    onSetParent parent, child, builder
  }

  /**
   *  Handle set parent.
   *
   * @param parent the parent
   * @param node the node
   * @param builder the builder
   */
  @SuppressWarnings("GrMethodMayBeStatic")
  protected void onSetParent( BNode parent, BNode<T> node, FBS builder ) {
    node.parent = parent
  }

  /**
   * Called by FBS to notify parent about child
   * @param builder
   * @param _parent
   * @param _child
   */
  @Override
  public void setChild( FBS builder, Object _parent, Object _child ) {
    def (BNode<T> child, BNode<T> parent) = sanityCheck( _child, _parent )
    assert parent.nodeClass == clazz, "Bad parent or child found"
    onSetChild parent, child, builder
  }

  /**
   *  Handle set child.
   *
   * @param node the node
   * @param child the child
   * @param builder the builder
   */
  protected void onSetChild( BNode<T> node, BNode child, FBS builder ) {
    assert child.isBean || childTypes.any { it.key.isAssignableFrom child.nodeClass },
        "Invalid child found: $child ($node)"
    node.onSetChild child, builder
  }

  /**
   * Called to finalize node
   * @param builder
   * @param parent
   * @param node
   */
  public void onNodeCompleted( FBS builder, Object parent, Object node ) {
    completeInit parent as BNode, node as BNode, builder
  }

  /**
   *  Handle node completed.
   *
   * @param parent the parent
   * @param node the node
   * @param builder the builder
   */
  protected void completeInit( BNode parent, BNode<T> node, FBS builder ) {
    if ( !initAfterParent ) {
      node.completeInit()
    }

    node.children.findAll { !it.initialized }.each { it.completeInit() }
    addChildren( node )
  }

  /**
   *  Add children to parent
   *
   * @param node the node
   */
  def addChildren( BNode<T> node ) {
    childTypes.each { type ->
      if ( type?.value?.containsKey( "addChild" ) ) {
        String method = type.value[ "addChild" ]
        node.findChildrenOfType( type.key ).each { child ->
//          log.info "completeInit: invoking ${method}($child) on $node"
          InvokerHelper.invokeMethod node.value, method, child.value
        }
      }
    }
  }

  static def sanityCheck( _child, _parent ) {
    assert _parent instanceof BNode && _child instanceof BNode, "Bad parent or child found"
    [ _child as BNode<T>, _parent as BNode<T> ]
  }
}