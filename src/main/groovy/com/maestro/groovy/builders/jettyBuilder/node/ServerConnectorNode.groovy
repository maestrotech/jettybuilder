/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder.node

import com.maestro.groovy.builders.jettyBuilder.BNode
import org.eclipse.jetty.server.ConnectionFactory
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.util.ssl.SslContextFactory

@SuppressWarnings("GroovyUnusedDeclaration")
class ServerConnectorNode extends BNode<ServerConnector> {

  /**
   * Constructor
   * @param name
   * @param value
   * @param map
   * @param c
   */
  ServerConnectorNode( final Object name, final Object value, final Map map, final Class<ServerConnector> c ) {
    super( name, value, map, c )
  }

  /**
   * Complete initialization
   * @param args
   * @return
   */
  ServerConnector completeInit( Object... args ) {
    assert args.length == 1 && args[ 0 ].class == Server, "Invalid arg[0] (must be of type Server)"

    def server = args[ 0 ]
    def sslConfig = findChildOfType( SslContextFactory )
    def connections = findChildrenOfType( ConnectionFactory ).collect { it.value }
    def c = connections as ConnectionFactory[]

    [ !connections.empty, sslConfig?.value != null ].match {
      when( [ false, false ] ) then { super.completeInit server }
      when( [ false, true ] ) then { super.completeInit server, sslConfig.value }
      when( [ true, false ] ) then { super.completeInit server, c }
      when( [ true, true ] ) then { super.completeInit server, sslConfig.value, c }
    } as ServerConnector
  }
}
