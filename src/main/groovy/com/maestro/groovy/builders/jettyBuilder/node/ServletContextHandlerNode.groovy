/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder.node

import com.maestro.groovy.builders.jettyBuilder.BNode
import org.eclipse.jetty.servlet.ServletContextHandler

@SuppressWarnings("GroovyUnusedDeclaration")
class ServletContextHandlerNode extends BNode<ServletContextHandler> {

  def useSecurity = false
  def useSessions = false

  ServletContextHandlerNode( final Object name, final Object value, final Map map, final Class<ServletContextHandler> c ) {
    super( name, value, map, c )
    useSecurity = attributes.remove( "useSecurity" )
    useSessions = attributes.remove( "useSessions" )
  }

  ServletContextHandler completeInit( Object... args ) {
    int flags = 0
    flags += useSecurity ? ServletContextHandler.SECURITY : 0
    flags += useSessions ? ServletContextHandler.SESSIONS : 0
    super.completeInit flags
  }
}
