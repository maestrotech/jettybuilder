/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder.node

import com.maestro.groovy.builders.jettyBuilder.BNode
import org.eclipse.jetty.server.SslConnectionFactory
import org.eclipse.jetty.util.ssl.SslContextFactory

@SuppressWarnings("GroovyUnusedDeclaration")
class SslConnectionFactoryNode extends BNode<SslConnectionFactory> {

  def nextProtocol = null

  SslConnectionFactoryNode( final Object name, final Object value, final Map map, final Class<SslConnectionFactory> c ) {
    super( name, value, map, c )
    nextProtocol = attributes.remove( "nextProtocol" )
  }

  SslConnectionFactory completeInit( Object... args ) {
    def conn = findChildOfType( SslContextFactory )
    [ conn != null, nextProtocol != null ].match {
      when( [ true, true ] ) then { super.completeInit conn.value, nextProtocol }
      when( [ false, true ] ) then { super.completeInit nextProtocol }
      otherwise { super.completeInit() }
    } as SslConnectionFactory
  }
}
