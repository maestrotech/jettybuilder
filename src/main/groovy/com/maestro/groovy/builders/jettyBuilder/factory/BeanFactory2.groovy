/**
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.maestro.groovy.builders.jettyBuilder.factory

import com.maestro.groovy.builders.jettyBuilder.BNode

@SuppressWarnings("GroovyUnusedDeclaration")
class BeanFactory2 extends BaseFactory<Object> {
  /**
   *  Instantiates a new Http connection factory factory.
   */
  BeanFactory2( ) {
    super( null )
  }

  BeanFactory2( Class clazz ) {
    super( clazz )
  }

  /**
   * Override for special bean handling
   * @param name
   * @param value
   * @param map
   * @param fbs
   * @return
   */
  protected BNode<Object> onNewInstance( String name, Object value, Map map, FactoryBuilderSupport fbs ) {
    def clazz = this.clazz

    if ( !clazz ) {
      String c = map.remove( "class" )

      String className = [ value != null, c != null ].match {
        when( [ true, false ] ) then value
        when( [ false, true ] ) then c
        otherwise null
      }

      assert className != null, "One and only one of either a literal value or named argument 'class' must be set "
      clazz = Class.forName( className )
    }

    def node = new BNode( name, value, map, clazz )
    node.isBean = true
    node
  }
}