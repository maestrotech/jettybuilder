package com.maestro.logger

import org.apache.log4j.Logger

import static org.apache.log4j.Logger.getLogger

/**
 * Copyright (c) 2012-13 by Author(s). All Right Reserved.
 * Author(s): venkat
 */
@SuppressWarnings("GroovyUnusedDeclaration")
public class LOGGER {
  Logger logger

  LOGGER( Class c ) { logger = getLogger( c ) }

  def i( def msg ) { logger.info msg }
  def i( def msg, Throwable e ) { logger.info msg, e }

  def e( def msg ) { logger.error msg }
  def e( def msg, Throwable e ) { logger.error msg, e }

  def d( def msg ) { logger.debug msg }
  def d( def msg, Throwable e ) { logger.debug msg, e }

  def w( def msg ) { logger.warn msg }
  def w( def msg, Throwable e ) { logger.warn msg, e }

  def f( def msg ) { logger.trace msg }
  def f( def msg, Throwable e ) { logger.trace msg, e }
}
