/*
 * Copyright 2012-13 Maestro Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author(s): venkat
 */

package com.maestro.groovy.builders.jettyBuilder

import groovy.swing.SwingBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.eclipse.jetty.server.Server

//class JettyBuilderTest extends GroovyTestCase {
class JettyBuilderTest  {
  String host = "localhost"
  String message = "hello WORLD"
  int port = 8095
  String userAgent = "Mozilla/5.0 Firefox/3.0.4"
  String contentType = "text/html; charset=UTF-8"
  Map<Integer, String> responses = [ ( 200 ): "HTTP/1.1 200 OK" ]

  JettyBuilder jetty
  Server server

  void setUp( ) {
//    BasicConfigurator.configure( new ConsoleAppender( new EnhancedPatternLayout( "%-2p %d{HH:mm:ss:SSS} %25.25c{1.}: %m%n" ) ) )
    jetty = new JettyBuilder()
    SwingBuilder
  }

  void test1( ) {

    x = jetty.server( id: "abc" ) {
      connector port: 8097, idleTimeout: 30000
      handlerList {
        resourceHandler directoriesListed: true, welcomeFiles: [ "abc" ], resourceBase: "."
        defaultHandler()
      }
    }

    x.value
  }

  @SuppressWarnings("GroovyAssignabilityCheck")
  void testSimpleScript( ) {

    server = JettyBuilder.fromScript( "src/test/groovy/com/maestro/groovy/builders/jettyBuilder/context.getty",
        [ message: message, host: host, port: port ] ) as Server
    assert server, "Server is null!"
    server.start()

    def html = new HTTPBuilder().request( "http://$host:$port/", Method.GET, ContentType.HTML ) { req ->
      uri.path = '/'
      headers.'User-Agent' = userAgent

      response.success = { resp, reader ->
        assertEquals 200, resp.statusLine.statusCode
        assertEquals message, reader.toString()
      }

      response.'404' = {
        assert false, "Got 404"
      }
    }

    server.stop()
    server.join()
  }

  @SuppressWarnings("GroovyAssignabilityCheck")
  void testManyContexts( ) {

    server = JettyBuilder.fromScript( "src/test/groovy/com/maestro/groovy/builders/jettyBuilder/manyContexts.getty",
        [ message: message, host: host, port: port ] ) as Server
    assert server, "Server is null!"
    server.start()

    [ "/": message,
        "/test1": message + message,
        "/test2": message + message + message ].each { p, m ->
      new HTTPBuilder().request( "http://$host:$port/", Method.GET, ContentType.HTML ) { req ->
        uri.path = p
        headers.'User-Agent' = userAgent

        response.success = { resp, reader ->
          assertEquals 200, resp.statusLine.statusCode
          assertEquals m, reader.toString()
        }

        response.'404' = { assert false, "Got 404" }
      }
    }

    server.stop()
    server.join()
  }

  @SuppressWarnings("GroovyAssignabilityCheck")
  void testServletContextHolder( ) {

    server = JettyBuilder.fromScript( "src/test/groovy/com/maestro/groovy/builders/jettyBuilder/servletContextHandler" +
        ".getty", [ message: message, host: host, port: port ] ) as Server
    assert server, "Server is null!"
    server.start()

    new HTTPBuilder().request( "http://$host:$port/", Method.GET, ContentType.HTML ) { req ->
      uri.path = "/tmp"
      headers.'User-Agent' = userAgent

      response.success = { resp, reader ->
        assertEquals 200, resp.statusLine.statusCode
        //assertEquals m, reader.toString()
        println reader
      }

      response.'404' = {
//        assert false, "Got 404"
      }
    }

    //server.stop()
    server.join()
  }
}
