package com.maestro.groovy.builders.jettyBuilder

import org.eclipse.jetty.server.Server

/**
 * Copyright (c) 2012-13 by Author(s). All Right Reserved.
 * Author(s): venkat
 */
class TestExt extends GroovyTestCase {

  void test1( ) {
    def server = JettyBuilder.fromScript( "src/test/groovy/com/maestro/groovy/builders/jettyBuilder/testExt.getty",
        [ "connectorConfig.port": 8096, "a.b.c.d": 200 ]
    ) as Server
    assert server, "Server is null!"
  }
}
